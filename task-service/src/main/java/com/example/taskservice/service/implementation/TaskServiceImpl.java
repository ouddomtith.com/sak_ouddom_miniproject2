package com.example.taskservice.service.implementation;
import com.example.common.model.Group;
import com.example.common.model.User;
import com.example.taskservice.model.Task;
import com.example.taskservice.model.TaskDto;
import com.example.taskservice.model.TaskRequest;
import com.example.taskservice.repository.TaskRepository;
import com.example.taskservice.service.TaskService;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.representations.AccessTokenResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class TaskServiceImpl implements TaskService {

    @Value("${keycloak.credentials.username}")
    private String username;
    @Value("${keycloak.credentials.password}")
    private String password;
    @Value("${keycloak.credentials.secret}")
    private String secretKey;
    @Value("${keycloak.resource}")
    private String clientId;
    @Value("${keycloak.auth-server-url}")
    private String authUrl;
    @Value("${keycloak.realm}")
    private String realm;

    private final TaskRepository repository;
    private final WebClient webClient;

    public TaskServiceImpl(TaskRepository repository, WebClient.Builder webClient) {
        this.repository = repository;
        this.webClient = webClient.baseUrl("http://localhost:8181").build();
    }

    public AccessTokenResponse returnToken(){

        Keycloak keycloak = KeycloakBuilder.builder()
                .serverUrl(authUrl)
                .realm(realm)
                .grantType(OAuth2Constants.PASSWORD)
                .clientId(clientId)
                .clientSecret(secretKey)
                .username(username)
                .password(password)
                .build();
        AccessTokenResponse tok = keycloak.tokenManager().getAccessToken();
        return tok;

    }

    @Override
    public TaskDto addTask(TaskRequest request) {

        Task task = repository.save(request.toEntity());

        User user = webClient.get()
                .uri("/users/{id}", task.getAssignTo())
                .headers(h -> h.setBearerAuth(returnToken().getToken()))
                .retrieve()
                .bodyToMono(User.class)
                .block();

        User createdBy = webClient.get()
                .uri("/users/{id}", task.getCreatedBy())
                .headers(h -> h.setBearerAuth(returnToken().getToken()))
                .retrieve()
                .bodyToMono(User.class)
                .block();

        Group group = webClient.get()
                .uri("/groups/{id}", task.getGroupId())
                .headers(h -> h.setBearerAuth(returnToken().getToken()))
                .retrieve()
                .bodyToMono(Group.class)
                .block();

        TaskDto dto = new TaskDto();
        dto.setId(task.getId());
        dto.setTitle(task.getTitle());
        dto.setDescription(task.getDescription());
        dto.setCreatedBy(createdBy);
        dto.setAssignTo(user);
        dto.setGroupId(group);
        dto.setCreatedDate(task.getCreatedDate());
        dto.setLastModified(task.getLastModified());
        return dto;
    }

    @Override
    public List<TaskDto> getAllTask() {

        List<Task> getAllTaskRepo = repository.findAll();
        List<TaskDto> responses = new ArrayList<>();
        for (Task task: getAllTaskRepo) {

            User user = webClient.get()
                    .uri("/users/{id}", task.getAssignTo())
                    .headers(h -> h.setBearerAuth(returnToken().getToken()))
                    .retrieve()
                    .bodyToMono(User.class)
                    .block();

            User createdBy = webClient.get()
                    .uri("/users/{id}", task.getCreatedBy())
                    .headers(h -> h.setBearerAuth(returnToken().getToken()))
                    .retrieve()
                    .bodyToMono(User.class)
                    .block();

            Group group = webClient.get()
                    .uri("/groups/{id}", task.getGroupId())
                    .headers(h -> h.setBearerAuth(returnToken().getToken()))
                    .retrieve()
                    .bodyToMono(Group.class)
                    .block();

            TaskDto dto = new TaskDto();
            dto.setId(task.getId());
            dto.setTitle(task.getTitle());
            dto.setDescription(task.getDescription());
            dto.setCreatedBy(createdBy);
            dto.setAssignTo(user);
            dto.setGroupId(group);
            dto.setCreatedDate(task.getCreatedDate());
            dto.setLastModified(task.getLastModified());
            responses.add(dto);

        }
        return responses;
    }

    @Override
    public TaskDto getTaskById(UUID id) {

        Task task = repository.findById(id).get();
        User user = webClient.get()
                .uri("/users/{id}", task.getAssignTo())
                .headers(h -> h.setBearerAuth(returnToken().getToken()))
                .retrieve()
                .bodyToMono(User.class)
                .block();

        User createdBy = webClient.get()
                .uri("/users/{id}", task.getCreatedBy())
                .headers(h -> h.setBearerAuth(returnToken().getToken()))
                .retrieve()
                .bodyToMono(User.class)
                .block();

        Group group = webClient.get()
                .uri("/groups/{id}", task.getGroupId())
                .headers(h -> h.setBearerAuth(returnToken().getToken()))
                .retrieve()
                .bodyToMono(Group.class)
                .block();

        TaskDto dto = new TaskDto();
        dto.setId(task.getId());
        dto.setTitle(task.getTitle());
        dto.setDescription(task.getDescription());
        dto.setCreatedBy(createdBy);
        dto.setAssignTo(user);
        dto.setGroupId(group);
        dto.setCreatedDate(task.getCreatedDate());
        dto.setLastModified(task.getLastModified());
        return dto;

    }

    @Override
    public TaskDto updateTaskById(UUID id, TaskRequest request) {

        Task task = repository.findById(id).get();
        User user = webClient.get()
                .uri("/users/{id}", request.getAssignTo())
                .headers(h -> h.setBearerAuth(returnToken().getToken()))
                .retrieve()
                .bodyToMono(User.class)
                .block();

        User createdBy = webClient.get()
                .uri("/users/{id}", request.getCreatedBy())
                .headers(h -> h.setBearerAuth(returnToken().getToken()))
                .retrieve()
                .bodyToMono(User.class)
                .block();

        Group group = webClient.get()
                .uri("/groups/{id}", request.getGroupId())
                .headers(h -> h.setBearerAuth(returnToken().getToken()))
                .retrieve()
                .bodyToMono(Group.class)
                .block();

        // Assign New Task
        task.setTitle(request.getTitle());
        task.setDescription(request.getDescription());
        task.setCreatedBy(request.getCreatedBy());
        task.setAssignTo(request.getAssignTo());
        task.setGroupId(request.getGroupId());
        task.setCreatedDate(task.getCreatedDate());
        task.setLastModified(request.toEntity().getLastModified());

        // Update Entity
        Task taskResponse = repository.save(task);

        TaskDto dto = new TaskDto();
        //Implement Dto
        dto.setId(taskResponse.getId());
        dto.setTitle(taskResponse.getTitle());
        dto.setDescription(taskResponse.getDescription());
        dto.setCreatedBy(createdBy);
        dto.setAssignTo(user);
        dto.setGroupId(group);
        dto.setCreatedDate(taskResponse.getCreatedDate());
        dto.setLastModified(taskResponse.getLastModified());

        return dto;
    }

    @Override
    public String deleteTaskById(UUID id) {
        repository.deleteById(id);
        return "Delete Successfully.";
    }
}
