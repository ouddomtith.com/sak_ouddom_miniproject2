package com.example.taskservice.service;
import com.example.taskservice.model.TaskDto;
import com.example.taskservice.model.TaskRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface TaskService {

    TaskDto addTask(TaskRequest request);

    List<TaskDto> getAllTask();

    TaskDto getTaskById(UUID id);

    TaskDto updateTaskById(UUID id, TaskRequest request);

    String deleteTaskById(UUID id);
}
