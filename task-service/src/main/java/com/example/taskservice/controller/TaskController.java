package com.example.taskservice.controller;

import com.example.taskservice.model.TaskDto;
import com.example.taskservice.model.TaskRequest;
import com.example.taskservice.service.TaskService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping
@SecurityRequirement(name = "miniproject02")
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }
    @PostMapping("/tasks")
    public TaskDto addTask(@RequestBody TaskRequest request){
        return taskService.addTask(request);
    }

    @GetMapping("tasks")
    public List<TaskDto> getAllTask(){
        return taskService.getAllTask();
    }

    @GetMapping("/tasks/{id}")
    public TaskDto getTaskById(@PathVariable UUID id){
        return taskService.getTaskById(id);
    }

    @PutMapping("/tasks/{id}")
    public TaskDto updateTaskById(@PathVariable UUID id,
                               @RequestBody TaskRequest request){
        return taskService.updateTaskById(id,request);
    }

    @DeleteMapping("/tasks/{id}")
    public String deleteTaskById(@PathVariable UUID id){
        return taskService.deleteTaskById(id);
    }
}
