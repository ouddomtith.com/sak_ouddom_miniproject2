package com.example.taskservice.model;

import com.example.common.model.Group;
import com.example.common.model.User;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tasks")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    private String title;
    private String description;
    private UUID createdBy;
    private UUID assignTo;
    private UUID groupId;
    private LocalDateTime createdDate;
    private LocalDateTime lastModified;

}
