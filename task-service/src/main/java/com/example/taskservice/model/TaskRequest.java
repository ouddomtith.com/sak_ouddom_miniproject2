package com.example.taskservice.model;
import com.example.common.model.Group;
import com.example.common.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskRequest {

    private String title;
    private String description;
    private UUID createdBy;
    private UUID assignTo;
    private UUID groupId;

    public Task toEntity(){
        return new Task(null,title,description,createdBy,assignTo,groupId,LocalDateTime.now(), LocalDateTime.now());
    }

}
