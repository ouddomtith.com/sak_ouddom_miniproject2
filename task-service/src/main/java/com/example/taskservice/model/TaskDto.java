package com.example.taskservice.model;

import com.example.common.model.Group;
import com.example.common.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {

    private UUID id;
    private String title;
    private String description;
    private User createdBy;
    private User assignTo;
    private Group groupId;
    private LocalDateTime createdDate;
    private LocalDateTime lastModified;

}
