//package com.example.apigateway;
//
//import org.springdoc.core.models.GroupedOpenApi;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.gateway.route.RouteDefinition;
//import org.springframework.cloud.gateway.route.RouteDefinitionLocator;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Lazy;
//import reactor.core.publisher.Flux;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Configuration
//public class SwaggerConfig {
//
//    @Autowired
//    private RouteDefinitionLocator locator;
//
//    @Bean
//    @Lazy(false)
//    public List<GroupedOpenApi> apis() {
//        List<GroupedOpenApi> groups = new ArrayList<>();
//        Flux<RouteDefinition> routeDefinitions = locator.getRouteDefinitions();
//
//        routeDefinitions.toIterable().forEach(routeDefinition -> {
//            String serviceName = extractServiceName(routeDefinition.getId());
//            GroupedOpenApi group = GroupedOpenApi.builder()
//                    .pathsToMatch("/" + serviceName + "/**")
//                    .group(serviceName)
//                    .build();
//            groups.add(group);
//        });
//
//        return groups;
//    }
//
//    private String extractServiceName(String routeId) {
//        return routeId.replaceAll("-route", "");
//    }
//}
//
//
