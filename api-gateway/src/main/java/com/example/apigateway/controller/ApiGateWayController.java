package com.example.apigateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api-gateway")
public class ApiGateWayController {
    @GetMapping
    public String apiGateWay(){
        return "Api Gateway";
    }
}
