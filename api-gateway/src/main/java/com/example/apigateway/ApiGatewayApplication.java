package com.example.apigateway;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@OpenAPIDefinition(
        servers = @Server(url = "/", description = "Default Server URL"),
        info = @Info(
                title = "ApiGateWay",
                description = "Started From 26/09/2023",
                version = "1.0.0"
        )
)
public class ApiGatewayApplication {
	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayApplication.class, args);
	}

	@Bean
	public RouteLocator routeLocator(RouteLocatorBuilder builder) {
		return builder
				.routes()
				.route(r -> r.path("/task-service/v3/api-docs").uri("lb://task-service"))
				.route(r -> r.path("/tasks/**").uri("lb://task-service"))
				.route(r -> r.path("/keycloak-service/v3/api-docs").uri("lb://keycloak-service"))
				.route(r -> r.path("/users/**").uri("lb://keycloak-service"))
				.route(r -> r.path("/groups/**").uri("lb://keycloak-service"))
				.build();
	}
}
